import { _decorator, Component, math, Node } from 'cc'
import { BuffType, ConflictResolution } from '../Enum/Index'
const { ccclass, property } = _decorator

@ccclass('BuffBase')
export default class BuffBase {
  //buff的拥有者
  private _owner: Node = null

  //buff来源于哪一个类，使用工厂函数生成buff之后需要提供类名，来判断buff是否相等
  protected _originClass: string = 'BuffBase'

  //buff的提供者
  private _provider: string = ''

  //buff最大的持续时间
  private _maxDuration: number = 3

  //buff的流逝速度，最小为0，最大为10。
  private _timeScale: number = 1

  //buff最大层数
  private _maxLevel: number = 1

  //buff的类型 分为正面、负面、中立三种
  private _buffType: BuffType = BuffType.None

  //buff的叠加机制
  //当多个不同单位向同一个单位施加同一个buff时的冲突处理
  /// 分为三种：
  /// combine,合并为一个buff，叠层（提高等级）
  /// separate,独立存在
  /// cover, 覆盖，后者覆盖前者
  private _conflictResolution: ConflictResolution = ConflictResolution.Cover

  //buff是否被驱散
  private _dispellable: boolean = true

  //buff的名称
  private _name: string = '默认名称'

  //buff的技能描述
  private _description: string = '默认描述'

  //每次Buff持续时间结束时降低的等级，一般降低1级或者降低为0级。
  private _demotion: number = 1

  // 图标资源的路径
  private _iconPath: string = ''

  //Buff的当前等级
  private _currentLevel: number = 0

  //Buff的当前剩余时间
  private _residualDuration: number = 3
  private _init: boolean = false

  public get Owner(): Node {
    return this._owner
  }

  public set Owner(value: Node) {
    this._owner = value
  }

  public get Provider(): string {
    return this._provider
  }

  public set Provider(value: string) {
    this._provider = value
  }

  public get MaxDuration(): number {
    return this._maxDuration
  }

  public set MaxDuration(value: number) {
    this._maxDuration = value
  }

  public get TimeScale(): number {
    return this._timeScale
  }

  public set TimeScale(value: number) {
    this._timeScale = math.clamp(value, 0, 10)
  }

  public get MaxLevel(): number {
    return this._maxLevel
  }

  public set MaxLevel(value: number) {
    this._maxLevel = value
  }

  public get BuffType(): BuffType {
    return this._buffType
  }

  public set BuffType(value: BuffType) {
    this._buffType = value
  }

  public get ConflictResolution(): ConflictResolution {
    return this._conflictResolution
  }

  public set ConflictResolution(value: ConflictResolution) {
    this._conflictResolution = value
  }

  public get Dispellable(): boolean {
    return this._dispellable
  }

  public set Dispellable(value: boolean) {
    this._dispellable = value
  }

  public get Name(): string {
    return this._name
  }

  protected set Name(value: string) {
    this._name = value
  }

  public get Description(): string {
    return this._description
  }

  public set Description(value: string) {
    this._description = value
  }

  public get IconPath(): string {
    return this._iconPath
  }

  public set IconPath(value: string) {
    this._iconPath = value
  }

  public get Demotion(): number {
    return this._demotion
  }

  public set Demotion(value: number) {
    this._demotion = math.clamp(value, 0, this.MaxLevel)
  }

  public get CurrentLevel(): number {
    return this._currentLevel
  }

  public set CurrentLevel(value: number) {
    let change: number = math.clamp(value, 0, this.MaxLevel) - this._currentLevel
    this.onLevelChange(change)
    this._currentLevel += change
  }

  public get ResidualDuration(): number {
    return this._residualDuration
  }

  public set ResidualDuration(value: number) {
    this._residualDuration = math.clamp(value, 0, Number.MAX_VALUE)
  }

  public get OriginClass(): string {
    return this._originClass
  }

  public set OriginClass(originClass: string) {
    this._originClass = originClass
  }

  //当Owner获得此buff时触发
  //由BuffManager在合适的时候调用
  public onGet(): void {}

  /// 当Owner失去此buff时触发
  /// 由BuffManager在合适的时候调用
  public onLost(): void {}

  /// Update,由BuffManager每帧调用
  public update(): void {}

  /// 当等级改变时调用
  public onLevelChange(change: number) {}

  /// 初始化
  public initialize(buffClass: string, owner: Node, provider: string): void {
    if (this._init) {
      console.error('不能对已经初始化的buff再次初始化')
    }
    if (owner == null || provider == null) {
      console.error('初始化值不能为空')
    }
    this._owner = owner
    this._provider = provider
    this._originClass = buffClass
    this._init = true
  }
}
