import { Node } from 'cc'
import BuffBase from './BuffBase'
import Singleton from './Singleton'
import { Buff001 } from '../Script/Buff001'

export class BuffFactory extends Singleton {
  public static get Instance() {
    return super.GetInstance<BuffFactory>()
  }

  public CreateBuff(classType: string, target: Node, provider: string, level: number = 1): BuffBase {
    if (classType === 'BuffBase') {
      return new BuffBase()
    } else if (classType === 'Buff001') {
      return new Buff001()
    }
  }
}
