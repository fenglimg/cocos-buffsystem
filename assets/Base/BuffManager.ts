import { _decorator, Component, Node, Scheduler } from 'cc'
import BuffBase from './BuffBase'
import { ConflictResolution, Event_Enum } from '../Enum/Index'
import EventManager from './EventManager'
import { BuffFactory } from './BuffFactory'
const { ccclass } = _decorator
export const FixedUpdateTime = 0.5
const GarbageCollectionTime = 10
@ccclass('BuffManager')
export class BuffManager extends Component {
  //单例

  private constructor() {
    super()
  }
  private buffMap: Map<Node, Array<BuffBase>> = new Map<Node, Array<BuffBase>>()
  private observers: Array<Node> = new Array<Node>()
  private buffMapCopy: Map<Node, Array<BuffBase>> = new Map<Node, Array<BuffBase>>()

  private garbageCollectionTime: number = 0
  private fixedUpdateTime: number = 0

  /**
   * 返回要观察的对象现有的buff，并且在对象被添加新buff时通知你
   * （如果现在对象身上没有buff会返回空列表，不会返回null）
   */

  public startObserving(target: Node): Array<BuffBase> {
    let list: Array<BuffBase>

    const foundItem = this.observers.find((item) => item === target)
    // 添加监听

    if (!foundItem) {
      this.observers.push(target)
    }

    // 查找已有buff
    if (this.buffMap.has(target)) {
      list = this.buffMap.get(target)
    } else {
      list = []
    }
    console.log('list: ', list)

    // 返回
    return list
  }
  /**
   * 停止观察某一对象，请传入与调用开始观察方法时使用的相同参数。
   */
  public stopObserving(target: Node): void {
    const index = this.observers.findIndex((item) => item === target)
    if (index === -1) {
      throw new Error('要停止观察的对象不存在')
    }

    this.observers.splice(index, 1)
  }

  public addBuff(buffClassType: string, target: Node, provider: string, level: number = 1): void {
    // 如果字典中不存在目标，则进行初始化
    if (!this.buffMap.has(target)) {
      this.buffMap.set(target, Array<BuffBase>())
      // 目标身上没有任何 buff，直接添加新的 buff
      this.addNewBuff(buffClassType, target, provider, level)
      console.log('直接添加buff')
      console.log('this.buffMap: ', this.buffMap)
      return
    }
    console.log('this.buffMap: ', this.buffMap)

    const buffList = this.buffMap.get(target)

    // 如果目标身上没有任何 buff，直接添加新的 buff
    if (buffList.length === 0) {
      this.addNewBuff(buffClassType, target, provider, level)
      return
    }

    // 遍历目标身上已存在的 buff，检查是否有相同类型的 buff

    const existingBuffs: BuffBase[] = []
    for (const buff of buffList) {
      if (buff.OriginClass === buffClassType) {
        existingBuffs.push(buff)
      }
    }

    // 如果没有相同类型的 buff，直接添加新的 buff
    // 如果存在相同类型的 buff，则根据冲突处理策略进行处理
    if (existingBuffs.length === 0) {
      this.addNewBuff(buffClassType, target, provider, level)
    } else {
      const conflictResolution = existingBuffs[0].ConflictResolution

      switch (conflictResolution) {
        case ConflictResolution.Separate:
          let isProviderFound = false
          for (const buff of existingBuffs) {
            if (buff.Provider === provider) {
              buff.CurrentLevel += level
              isProviderFound = true
              break
            }
          }
          if (!isProviderFound) {
            this.addNewBuff(buffClassType, target, provider, level)
          }
          break

        case ConflictResolution.Combine:
          existingBuffs[0].CurrentLevel += level
          break

        case ConflictResolution.Cover:
          this.removeBuff(target, existingBuffs[0])
          this.addNewBuff(buffClassType, target, provider, level)
          break
      }
    }
  }

  private addNewBuff(buffClassType: string, target: Node, provider: string, level: number = 1): void {
    const buff = BuffFactory.Instance.CreateBuff(buffClassType, target, provider, level)
    buff.initialize(buffClassType, target, provider)
    const existingBuffs = this.buffMap.get(target)
    existingBuffs.push(buff)
    this.buffMap.set(target, existingBuffs)
    buff.ResidualDuration = buff.MaxDuration
    buff.CurrentLevel = level
    buff.onGet()
    const index = this.observers.findIndex((item) => item === target)
    if (index !== -1) {
      EventManager.Instance.emit(Event_Enum.OnBuffAdd, target, buff)
    }
    console.log(' this.buffMap: ', this.buffMap)
  }

  public FindBuff(buffClassType: string, owner: Node): BuffBase[] {
    const result: BuffBase[] = []
    if (this.buffMap.has(owner)) {
      const buffs = this.buffMap.get(owner)
      for (const buff of buffs) {
        if (buff.OriginClass === buffClassType) {
          result.push(buff)
        }
      }
    }
    return result
  }

  public FindAllBuff(owner: Node): BuffBase[] {
    if (this.buffMap.has(owner)) {
      return this.buffMap.get(owner) || []
    }
    return []
  }

  public removeBuff(owner: Node, buff: BuffBase): boolean {
    if (!this.buffMap.has(owner)) {
      return false
    }

    const buffs = this.buffMap.get(owner)
    let haveTarget = false

    for (let i = buffs.length - 1; i >= 0; i--) {
      const item = buffs[i]
      if (item === buff) {
        haveTarget = true
        item.CurrentLevel -= item.CurrentLevel
        item.onLost()
        buffs.splice(i, 1)
        break
      }
      console.log('removeBuff: ')
    }

    if (!haveTarget) {
      return false
    }

    return true
  }
  private executeFixedUpdate(): void {
    // 执行所有 buff 的 update

    for (const [owner, buffs] of this.buffMap) {
      for (const buff of buffs) {
        if (buff.CurrentLevel > 0 && buff.Owner !== null) {
          buff.update()
        }
      }
    }
  }

  private executeGrabageCollection(): void {
    this.buffMapCopy.clear()

    for (const [key, value] of this.buffMap.entries()) {
      this.buffMapCopy.set(key, value)
    }

    // 清理无用对象
    for (const [key, value] of this.buffMapCopy.entries()) {
      // 如果 owner 被删除，我们这边也跟着删除
      if (key === null) {
        this.buffMap.delete(key)
        continue
      }

      // 如果一个 owner 身上没有任何 buff，就没必要留着他了
      if (value.length === 0) {
        this.buffMap.delete(key)
        continue
      }
    }
  }

  update(dt: number): void {
    console.log('this.buffMap: ', this.buffMap)

    for (const [owner, buffs] of this.buffMap) {
      // 清理无用 buff
      // 降低持续时间
      for (let i = buffs.length - 1; i >= 0; i--) {
        const buff = buffs[i]
        // 如果等级为0，则移除
        if (buff.CurrentLevel === 0) {
          this.removeBuff(owner, buff)
          continue
        }
        // 如果持续时间为0，则降级
        // 降级后如果等级为0则移除，否则刷新持续时间
        if (buff.ResidualDuration === 0) {
          buff.CurrentLevel -= buff.Demotion
          if (buff.CurrentLevel === 0) {
            this.removeBuff(owner, buff)
            continue
          } else {
            buff.ResidualDuration = buff.MaxDuration
          }
        }
        // 降低持续时间
        buff.ResidualDuration -= dt
      }
    }

    this.fixedUpdateTime -= dt
    if (this.fixedUpdateTime <= 0) {
      this.executeFixedUpdate()
      this.fixedUpdateTime = FixedUpdateTime
    }

    this.garbageCollectionTime -= dt
    if (this.garbageCollectionTime <= 0) {
      this.executeGrabageCollection()
      this.garbageCollectionTime = GarbageCollectionTime
    }
  }
}
