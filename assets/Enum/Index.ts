export enum BuffType {
  /// 正面buff
  Buff,
  /// 负面buff
  Debuff,
  /// 没有面buff
  None
}

export enum ConflictResolution {
  /// <summary>
  /// 合并为一个buff，叠层（提高等级）
  /// </summary>
  Combine,
  /// <summary>
  /// 独立存在
  /// </summary>
  Separate,
  /// <summary>
  /// 覆盖，后者覆盖前者
  /// </summary>
  Cover
}

export enum Event_Enum {
  OnBuffAdd = 'OnBuffAdd',
  OnBuffIconClick = 'OnBuffIconClick'
}

export enum Buff_Class_Type {
  BuffBase = 'BuffBase'
}

export interface CharacterData {
  HP: number
  AD: number
}
