import { _decorator, Component, Node } from 'cc'
import { Hero } from './Hero'
import BuffBase from '../Base/BuffBase'
import { BuffType, ConflictResolution } from '../Enum/Index'
import { FixedUpdateTime } from '../Base/BuffManager'
const { ccclass, property } = _decorator

@ccclass('Buff001')
export class Buff001 extends BuffBase {
  private healingPerSecond: number = 20
  private target: Hero = null
  public override initialize(buffClass: string, owner: Node, provider: string) {
    super.initialize(buffClass, owner, provider)
    this.target = owner.getComponent(Hero)
    this.MaxDuration = 5
    this.TimeScale = 1
    this.MaxLevel = 5
    this.BuffType = BuffType.Buff
    this.ConflictResolution = ConflictResolution.Combine
    this.Dispellable = false
    this.Name = '恢复体力'
    this.Description = '每秒恢复' + this.healingPerSecond + '点生命值'
    this.Demotion = 1
    this.IconPath = '2003/spriteFrame'
  }
  public override update() {
    this.target.HP += this.healingPerSecond * FixedUpdateTime
  }
}
