import { _decorator, Button, Component, error, Label, Node, NodePool, resources, Sprite, SpriteFrame } from 'cc'
import BuffBase from '../Base/BuffBase'
import EventManager from '../Base/EventManager'
import { Event_Enum } from '../Enum/Index'
import { PoolMgr } from '../Base/PoolMgr'
const { ccclass, property } = _decorator

@ccclass('BuffItem')
export class BuffItem extends Component {
  icon: Sprite = null

  mask: Sprite = null

  levelLabel: Label = null

  private isInit: boolean = false

  private needNumber: boolean = false

  private targetBuff: BuffBase = null

  onLoad() {
    this.icon = this.node.getComponent(Sprite)!
    this.mask = this.node.getChildByName('Mask').getComponent(Sprite)!
    this.levelLabel = this.node.getChildByName('Level').getComponent(Label)!
    this.node.getComponent(Button).node.on(Button.EventType.CLICK, this.onclick, this)
  }

  onclick() {
    EventManager.Instance.emit(Event_Enum.OnBuffIconClick, this.targetBuff, this.node.worldPosition)
  }

  async init(buff: BuffBase) {
    resources.load(buff.IconPath, SpriteFrame, (err, spriteFrame) => {
      if (err) {
        console.error(err)
        return
      }
      this.icon.spriteFrame = spriteFrame
    })

    this.targetBuff = buff

    if (this.targetBuff.MaxLevel > 1) {
      this.needNumber = true
      this.levelLabel.node.active = true
    } else {
      this.needNumber = false
      this.levelLabel.node.active = false
    }

    if (this.targetBuff.TimeScale > 0) {
      this.mask.node.active = true
    } else {
      this.mask.node.active = false
    }

    this.isInit = true
  }

  protected update(dt: number): void {
    if (this.isInit) {
      //需要显示计时工具才显示
      this.mask.fillRange = -(1 - this.targetBuff.ResidualDuration / this.targetBuff.MaxDuration)
      //需要显示等级才显示
      if (this.needNumber) {
        this.levelLabel.string = this.targetBuff.CurrentLevel.toString()
      }
      //如果当前等级等于零说明他已经被废弃了，所以就可以回收了
      if (this.targetBuff.CurrentLevel == 0) {
        PoolMgr.Instance.putNode(this.node)
      }
    }
  }

  unuse() {
    this.isInit = false
    this.needNumber = false
    this.targetBuff = null
    this.node.active = false
  }

  reuse() {
    this.node.active = true
  }
}
