import { _decorator, Component, Node } from 'cc'
import { CharacterData } from '../Enum/Index'
const { ccclass, property } = _decorator

@ccclass('Hero')
export class Hero extends Component implements CharacterData {
  HP: number = 100
  AD: number = 0
}
