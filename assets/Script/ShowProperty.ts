import { _decorator, Component, Label, Node } from 'cc'
import { CharacterData } from '../Enum/Index'
import { Hero } from './Hero'
const { ccclass, property } = _decorator

@ccclass('ShowProperty')
export class ShowProperty extends Component {
  @property(Label)
  hpLabel: Label = null!

  @property(Label)
  adLabel: Label = null!

  @property(Hero)
  hero: Hero = null!

  start(): void {}

  protected update(dt: number): void {
    this.hpLabel.string = '生命值: ' + `${this.hero.HP}`
    this.adLabel.string = '攻击力: ' + `${this.hero.AD}`
  }
}
