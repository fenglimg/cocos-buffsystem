import { _decorator, Component, EventKeyboard, Input, input, KeyCode, Node, Prefab } from 'cc'
import { BuffManager } from '../Base/BuffManager'
import { PoolMgr } from '../Base/PoolMgr'
import EventManager from '../Base/EventManager'
import { Event_Enum } from '../Enum/Index'
import BuffBase from '../Base/BuffBase'
import { BuffItem } from './BuffItem'
const { ccclass, property } = _decorator

@ccclass('Test')
export class Test extends Component {
  @property(Node)
  heroNode: Node = null!

  @property(Prefab)
  buffItem: Prefab = null!

  @property(Node)
  buffParentNode: Node = null!

  @property(Node)
  showInfoNode: Node = null!

  @property(BuffManager)
  buffManager: BuffManager = null!

  protected start(): void {
    PoolMgr.Instance.createPool(this.buffItem, 10, 'BuffItem')
    this.buffManager.startObserving(this.heroNode)
    EventManager.Instance.on(Event_Enum.OnBuffAdd, this.OnBuffAdd, this)
    input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this)
    input.on(Input.EventType.TOUCH_START, this.onTouchStart, this)
  }

  onKeyDown(event: EventKeyboard) {
    if (event.keyCode === KeyCode.KEY_Q) {
      this.buffManager.addBuff('Buff001', this.heroNode, 'God', 1)
      // BuffManager.Instance().addBuff()
    }
  }

  OnBuffAdd(target: Node, buff: BuffBase) {
    const buffItem = PoolMgr.Instance.getNode('BuffItem', this.buffParentNode)
    buffItem.getComponent(BuffItem).init(buff)
  }

  onTouchStart() {
    this.showInfoNode.active = false
  }
}
